# Correlate_gene_expression_with_metabolite_scripts

The phylogenetic walking analyses and expression quantification were conducted at the Mesabi compute cluster in Minnesota Supercomputing Institute (https://www.msi.umn.edu/content/mesabi) in Nov 2017. Other analyses were conducted using a workstation with 24 cores and 96 Gb RAM in Ya Yang’s lab, University of Minnesota Twin Cities. Updated in Feb 2019

###Step 1: Phylogenetic walking
The DNA and pep sequences for each species were obtained from Ya's former studies. To find out the homologous genes among species, protein sequence of 29086 transcripts of B. vulgaris (http://bvseq.boku.ac.at/Genome/Download/index.shtml; RefBeet-1.2) were used to identify homologous genes and construct gene trees using scripts from Lopez-Nieves et al. (2017) with minor edition.  Specifically, each protein sequence was used as ‘bait’ to search against other species separately with SWIPE v.2.0.12. A maximum of 8 hits were kept for each species per transcript. Next, homologous genes were aligned using MAFFT v.7.130 with parameter ‘--genafpair --maxiterate 1000’ if more than four genes were found. Aligned sites with missing ≥ 20% data were trimmed by Phyutility v.2.2.6. Gene tree was constructed using RAxML v.8.2.11 with model ‘PROTCATAUTO’. Finally, phylogenetic tips with long branch length were removed.
The major difference of the scripts used here compared to those in Lopez-Nieves et al. (2017) is that we here added an folder. The gene name will be deposited in the folder if error happen during the walking processing.

For each gene, the walking analysis may take 1 min (if no this were found by SWIPE) to 5 hours (if in total more than 400 hits were found). To finish all walking analyses in acceptable time, we have allocated baits genes to sub-folder, which include about 50 genes. 
Submit analysis jobs to Mesabi cluster using qsub. An example PBS script is as following:
```
#PBS -l walltime=96:00:00,nodes=1:ppn=24,mem=20000mb
#PBS -m abe
#PBS -M clingyun@umn.edu
cd /home/yangya/clingyun/Python_scripts/scripts_genome_walking_Ya_2017_Chen_raxmlAVX
module load /panfs/roc/soft/modulefiles.common/java/jdk1.8.0_45
module load /panfs/roc/soft/modulefiles.common/raxml/8.2.11_pthread
for a in /home/yangya/clingyun/data/phylogeny_metabolomics/Bv1/Bv1_1/*; do time python bait_homologs.py $a /home/yangya/clingyun/data/phylogeny_metabolomics/all_cdhit.pep/ 8 3 /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/Bv1_results /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/errordir_bv1; done & 
for a in /home/yangya/clingyun/data/phylogeny_metabolomics/Bv1/Bv1_2/*; do time python bait_homologs.py $a /home/yangya/clingyun/data/phylogeny_metabolomics/all_cdhit.pep/ 8 3 /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/Bv1_results /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/errordir_bv1; done & 
for a in /home/yangya/clingyun/data/phylogeny_metabolomics/Bv1/Bv1_3/*; do time python bait_homologs.py $a /home/yangya/clingyun/data/phylogeny_metabolomics/all_cdhit.pep/ 8 3 /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/Bv1_results /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/errordir_bv1; done & 
for a in /home/yangya/clingyun/data/phylogeny_metabolomics/Bv1/Bv1_4/*; do time python bait_homologs.py $a /home/yangya/clingyun/data/phylogeny_metabolomics/all_cdhit.pep/ 8 3 /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/Bv1_results /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/errordir_bv1; done & 
for a in /home/yangya/clingyun/data/phylogeny_metabolomics/Bv1/Bv1_5/*; do time python bait_homologs.py $a /home/yangya/clingyun/data/phylogeny_metabolomics/all_cdhit.pep/ 8 3 /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/Bv1_results /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/errordir_bv1; done & 
for a in /home/yangya/clingyun/data/phylogeny_metabolomics/Bv1/Bv1_6/*; do time python bait_homologs.py $a /home/yangya/clingyun/data/phylogeny_metabolomics/all_cdhit.pep/ 8 3 /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/Bv1_results /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/errordir_bv1; done & 
for a in /home/yangya/clingyun/data/phylogeny_metabolomics/Bv1/Bv1_7/*; do time python bait_homologs.py $a /home/yangya/clingyun/data/phylogeny_metabolomics/all_cdhit.pep/ 8 3 /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/Bv1_results /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/errordir_bv1; done & 
for a in /home/yangya/clingyun/data/phylogeny_metabolomics/Bv1/Bv1_8/*; do time python bait_homologs.py $a /home/yangya/clingyun/data/phylogeny_metabolomics/all_cdhit.pep/ 8 3 /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/Bv1_results /home/yangya/clingyun/data/phylogeny_metabolomics/bait_result_Nov2017/errordir_bv1; done & 
wait
```

Next, to identify orthologous genes, we scanned all genes trees to figure out clades with ‘bait’ and ≥ 24 Carphyllales species presented and no outgroup presented. In total 16987 orthologous gene groups were extracted. 
```
python extract_clades_chen_sep2018.py folder_include_gene_trees .tre output_dir minimum_no_ingroup code_file(taxon_code_3) suffix_output_file
```

Last, generate species tree:
```
python stag.py taxon_table folder_include_all_gene_trees
```
taxon_table is as following:
```
MJM3333* blue_Polygonaceae_Bistorta_bistortoides
NepSFB* blue_Nepenthaceae_Nepenthes_alata_SFB
NXTS* blue_Molluginaceae_Mollugo_verticillata
Osativa* outgroup_Poaceae_Oryza_sativa
```
MJM3333 is the species abbreviation in the gene tree, blue_Polygonaceae_Bistorta_bistortoides is the species name we want to show in species tree

###Step 2: Expression quantification
An PBS file for Salmon mapping is as following:
```
#PBS -l walltime=6:00:00,nodes=1:ppn=5,mem=20000mb
#PBS -m abe
#PBS -M clingyun@umn.edu
source ~/.profile
cd ~/data/paper2/60species_raw_reads
salmon quant -p 4 -i Ruha.cds.fa.salmon.index --libType IU --dumpEq --validateMappings -r SRR1266797_1.cor.p.fq.gz --output  Ruha_SRR1266797_salmon_map
salmon quant -p 4 -i MJM2667.cds.fa.salmon.index --libType IU --dumpEq --validateMappings -1 SRR6435318_1.cor.p.fq.gz -2 SRR6435318_2.cor.p.fq.gz --output SRR6435318_salmon_map
wait
```

Next, change the quant.sf name, :
```
mv XXXX_salmon_map/quant.sf XXXX_salmon_map_quant.sf
```

###Step 3: Generate expression file for statistic analyses
Run following command:
```
python extract_expression_phylogeny_feb2019.py folder_include_gene_expression_files(*.quant.sf)/ file_indicate_trait_for_species folder_include_gene_trees output_folder
```
The analysis will generate the expression file for each gene, for example Bv9_226140_myxh
```
tpm	pigment
blue_Ancistrocladaceae_Ancistrocladus_robertsoniorum	-3.32192809489	0
blue_Caryophyllaceae_Arenaria_serpyllifolia	-3.32192809489	0
blue_Caryophyllaceae_Cerastium_arvense	-3.32192809489	0
.........
```

###Step 4: Run statistic analyses
